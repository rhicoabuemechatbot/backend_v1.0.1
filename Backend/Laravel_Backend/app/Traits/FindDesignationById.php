<?php
namespace App\Traits;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Designation;
use Validator;

trait FindDesignationById
{
    public function findDesignationById($request){
        $designation_id = $request;
        $designation = Designation::where('id', $designation_id)->first()->designation;

        return $designation;
    }
}

<?php
namespace App\Traits;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Employee;

use Validator;

trait RegisterUser
{
    public function register($request, $employee_id) {

        $rules = [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'username'=>'required|string|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ];

        $messages = [
            'required' => 'The :attribute field is required',
            'unique:users' => 'The :attribute field is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            Employee::destroy($employee_id);
            return $validator->messages()->first();
        }

        $request_password = $request->toArray()['password'];

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request_password)]
                ));

        // return response()->json([
        //     'message' => 'User successfully registered',
        //     'user' => $user,
        //     'id' => $user->id
        // ], 201);

        return $user->id;
    }

}

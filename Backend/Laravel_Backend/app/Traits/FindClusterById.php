<?php
namespace App\Traits;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Clusters;

use Validator;

trait FindClusterById
{
    public function findClusterById($request){
        $cluster_id = $request;
        $cluster = Clusters::where('id', $cluster_id)->first()->clusters;

        return $cluster;
    }
}

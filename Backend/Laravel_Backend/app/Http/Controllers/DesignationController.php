<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Employee;
use App\Models\User;
use App\Models\Designation;
use App\Models\Clusters;

class DesignationController extends Controller
{
    public function storeDesignation(Request $request){
        $designation = Designation::create($request->all('designation'));
        return response()->json($designation, 200);
    }

    public function getAllDesignation(){
        $designation = Designation::all();
        // return $designation->pluck('designation');
        return response()->json($designation, 200);
    }

    public function updateDesignation(Request $request){
        $designation_id = $request->id;
        $designation = Designation::findOrFail($designation_id);
        $designation->designation = $request->designation;
        $designation->save();

        return response()->json([
            'message'=>'Designation successfully updated!',
            'data' => $designation
        ]);
    }

    public function deleteDesignation(Request $request){
        $designation_id = $request->id;
        Designation::destroy($designation_id);

        return response()->json([
            'message'=>'Designation successfully deleted!'
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{

    public function updateUserWithEmployeeId(Request $request, $id){
        $user = User::findOrFail($id);
        $user->employee_id = $request->employee_id;
        $user->save();

        return response()->json([
            'message'=>'User successfully updated!',
            'data' => $user
        ]);
    }

    public function findEmailByUsername(Request $request) {
        $username = $request->username;
        $email = User::where('username', $username)->first()->email;

        return response()->json($email, 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Employee;
use App\Models\User;
use App\Models\Designation;
use App\Models\Clusters;

class ClustersController extends Controller
{
    public function storeClusters(Request $request){
        $clusters = Clusters::create($request->all('clusters'));
        return response()->json($clusters, 200);
    }

    public function getAllClusters(){
        $clusters = Clusters::all();
        // return $clusters->pluck('clusters');
        return response()->json($clusters, 200);
    }

    public function updateCluster(Request $request){
        $clusters_id = $request->id;
        $clusters = Clusters::findOrFail($clusters_id);
        $clusters->clusters = $request->clusters;
        $clusters->save();

        return response()->json([
            'message'=>'Clusters successfully updated!',
            'data' => $clusters
        ]);
    }

    public function deleteCluster(Request $request){
        $clusters_id = $request->id;
        Clusters::destroy($clusters_id);

        return response()->json([
            'message'=>'Designation successfully deleted!'
        ]);
    }
}

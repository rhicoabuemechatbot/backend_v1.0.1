<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Time_Track extends Model
{
    protected $table = 'time_track';

    protected $fillable = [
        'name',
        'employee_id',
        'morning_time_in',
        'morning_time_out',
        'afternoon_time_in',
        'afternoon_time_out',
        'overtime_time_in',
        'overtime_time_out',
        'location',
        'work_mode'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Designation extends Model
{
    use Uuids;

    protected $table = 'designation';

    protected $fillable = [
        'designation',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}

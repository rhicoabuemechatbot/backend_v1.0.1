<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <style>
        *{
            margin: 5px !important;
        }

        .float-container {
            border: 3px solid #fff;
        }

        .float-child {
            width: 48%;
            float: left;
            border: 2px solid red;
        }

        .spacer{
            width: 5px;
            float: left;
        }

        table{
            width: 100%;
        }

        td {
            border: 1px solid black;
            text-align: center;
        }

        thead{
            text-align: center;
        }

        .center{
            text-align: center;
        }
    </style>
    <title></title>
  </head>
  <body>
    <div class="float-container">
        <div class="float-child">
            <p><strong>Civil Service Form No. 48</strong></p>
            <br>
            <div class="center">
                <h1>Daily Time Record</h1>
                <br>
                <h2 style="text-decoration: underline;">{{ $details }}</h2>
                <p>(Name)</p>
            </div>
            <br>
            <p>For the month of: <strong>{{ $month }} {{ $span_day }}, 2021</strong></p>
            <div class="center">
                <p>Official Hours for Arrival and Departure:</p>
                <strong>Regular Days</strong>
            </div>
            <br>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td rowspan="2"><b>Date</b></td>
                    <td colspan="2"><b>AM</b></td>
                    <td colspan="2"><b>PM</b></td>
                    <td colspan="2"><b>Overtime</b></td>
                </tr>
                <tr>
                    <td><b>ARR</b></td>
                    <td><b>DEP</b></td>
                    <td><b>ARR</b></td>
                    <td><b>DEP</b></td>
                    <td><b>ARR</b></td>
                    <td><b>DEP</b></td>
                </tr>
                </thead>
                <tbody>
                @for ($i = 0; $i <= 30; $i++)
                <tr>
                    <td>
                    {{ $i+1 }}
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->morning_time_in;

                                    $datetime_dateformat = new DateTime($employee[$i]->morning_time_in);
                                    $day = $datetime_dateformat->format('d');

                                    if($i+1 == $day){
                                        if($datetime == null){
                                            echo '';
                                        }else{
                                            $time = strtotime($datetime);
                                            $timeFormat = date('h:i A', $time);

                                            echo $timeFormat;
                                        }
                                    } else {
                                        echo '';
                                    }
                                } else{
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->morning_time_out;

                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->afternoon_time_in;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->afternoon_time_out;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->overtime_time_in;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->overtime_time_out;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                </tr>
                @endfor
                </tbody>
            </table>
            <div>
                <p>
                I Certify on my honor that the above is a true and correct report on the house work performed, record of which was made daily at the time of arrival at and departure from office.
                </p>
            </div>
            <br>
            <div class="center">
                <h2 style="text-decoration: underline;">{{ $details }}</h2>
                <p>Employee's signature</p>
            </div>
            <br>
            <br>
            <div class="center">
                <h2 style="text-decoration: underline;">_______________</h2>
                <p>Verify as to prescribed office hours</p>
            </div>
        </div>

        <div class="spacer"></div>

    </div>

    <div class="float-container">
        <div class="float-child">
            <p><strong>Civil Service Form No. 48</strong></p>
            <br>
            <div class="center">
                <h1>Daily Time Record</h1>
                <br>
                <h2 style="text-decoration: underline;">{{ $details }}</h2>
                <p>(Name)</p>
            </div>
            <br>
            <p>For the month of: <strong>{{ $month }} {{ $span_day }}, 2021</strong></p>
            <div class="center">
                <p>Official Hours for Arrival and Departure:</p>
                <strong>Regular Days</strong>
            </div>
            <br>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td rowspan="2"><b>Date</b></td>
                    <td colspan="2"><b>AM</b></td>
                    <td colspan="2"><b>PM</b></td>
                    <td colspan="2"><b>Overtime</b></td>
                </tr>
                <tr>
                    <td><b>ARR</b></td>
                    <td><b>DEP</b></td>
                    <td><b>ARR</b></td>
                    <td><b>DEP</b></td>
                    <td><b>ARR</b></td>
                    <td><b>DEP</b></td>
                </tr>
                </thead>
                <tbody>
                @for ($i = 0; $i <= 30; $i++)
                <tr>
                    <td>
                    {{ $i+1 }}
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->morning_time_in;

                                    $datetime_dateformat = new DateTime($employee[$i]->morning_time_in);
                                    $day = $datetime_dateformat->format('d');

                                    if($i+1 == $day){
                                        if($datetime == null){
                                            echo '';
                                        }else{
                                            $time = strtotime($datetime);
                                            $timeFormat = date('h:i A', $time);

                                            echo $timeFormat;
                                        }
                                    } else {
                                        echo '';
                                    }
                                } else{
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->morning_time_out;

                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->afternoon_time_in;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->afternoon_time_out;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->overtime_time_in;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                    <td>
                        @if (count($employee) > $i)
                            @php
                                if($employee[$i] != ''){
                                    $datetime = $employee[$i]->overtime_time_out;
                                    if($datetime == null){
                                        echo '';
                                    }else{
                                        $time = strtotime($datetime);
                                        $timeFormat = date('h:i A', $time);

                                        echo $timeFormat;
                                    }
                                } else {
                                    echo $employee[$i];
                                }
                            @endphp
                        @endif
                    </td>
                </tr>
                @endfor
                </tbody>
            </table>
            <div>
                <p>
                I Certify on my honor that the above is a true and correct report on the house work performed, record of which was made daily at the time of arrival at and departure from office.
                </p>
            </div>
            <br>
            <div class="center">
                <h2 style="text-decoration: underline;">{{ $details }}</h2>
                <p>Employee's signature</p>
            </div>
            <br>
            <br>
            <div class="center">
                <h2 style="text-decoration: underline;">_______________</h2>
                <p>Verify as to prescribed office hours</p>
            </div>
        </div>

        <div class="spacer"></div>

    </div>
  </body>
</html>
